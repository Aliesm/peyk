<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/register', 'Api\AuthController@register');
Route::post('/login', 'Api\AuthController@login');

//Route::apiResource('/product', 'Api\ProductContoller')->middleware('auth:api');

Route::middleware('auth:api')->group(function () {
    Route::group(['prefix' => 'product'], function()
    {
        Route::get('index', 'Api\product\ProductController@index');
        Route::post('add', 'Api\product\ProductController@store');
    });
    Route::group(['prefix' => 'sellers'], function()
    {
        Route::get('/all', 'Api\SellerController@index');
        Route::post('add', 'Api\SellerController@createSeller');
    });


    Route::post('address/add', 'Api\UserAddressController@store');

//    Route::resource('products', 'ProductController');
});
