<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\models\Address;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class SellerController extends Controller
{
    public function index(User $user)
    {

        if (Auth::user()->level != 2) return response(['error'=>'Authenticate error'], 403);

        $sellers = $user->where('is_seller' , 1)->with('addresses')->get();
        return response(['data', $sellers], 200);
    }

    public function createSeller(Request  $request,User $user, Address $address)
    {

        $validate = Validator::make($request->all(),[
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed',
             'city'  => 'required',
            'area'  => 'required',
            'user_id' => 'required',
            'district' => 'required',
            'str' => 'required',
            'number' => 'required',
        ]);

        if ($validate->errors()) return response(['errors', $validate->errors()], 415);

        $userLevel = Auth::user()->level;
        if ($userLevel != 2) return response(['error'=>'Authenticated Error'], 403);

       $userI = $user->create([
            'name'=> $request->name,
            'password'=> bcrypt($request->password),
            'number'=> $request->number,
            'cart_id'=> $request->cart_id,
            'national_code'=> $request->national_code,
            'email'=> $request->email,
        ]);

        $address->saveAddress($userI->id, $request->city, $request->area, $request->district, $request->str, $request->number,
                                $request->latitude, $request->longitude);

        return response(['data'=>'sellere is created'], 200);
    }
}
