<?php

namespace App\Http\Controllers\Api\product;

use App\Http\Controllers\Controller;
use App\models\Address;
use App\models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(Address $address, Product  $product){

//        $addres = $address->where(['user_id'=> Auth::id(), 'status'=>1])->select('city', 'area')->get();
//        $user   =  $address->whereIn('city',$addres->pluck('city'))
//                    ->whereIn('area',$addres->pluck('area'))
//                    ->where('user_id', '!=', Auth::id())->pluck('user_id');
//
//        $product = $product->whereIn('user_id', $user)->get();
//
//
//        return response(['data'=> $product], 200);


       $location = $address->where(['user_id'=>Auth::id(), 'status'=>1])->first();
       $loacte   =       DB::table("address");
       $loacte   =       $loacte->select("*", DB::raw("6371 * acos(cos(radians(" . $location->latitude . "))
                                * cos(radians(latitude)) * cos(radians(longitude) - radians(" . $location->longitude . "))
                                + sin(radians(" .$location->latitude. ")) * sin(radians(latitude))) AS distance"));
       $loacte   =       $loacte->having('distance', '<', 20);
       $loacte   =       $loacte->orderBy('distance', 'asc');
       $loacte   =       $loacte->get();

       return response(['data', $loacte], 200);
    }


    public function store(Request $request, Product $product)
    {

//                $validate = $request->validate([
//                    'name'  => 'required',
//                    'type'  => 'required',
//                    'dec' => 'required',
//                    'imgPath' => 'required',
//                    'count' => 'required',
//                    'price' => 'required',
//        ]);
//
//        if ($validate->error)return response(['eorros' => $validate->error, ], 403);

        $checkSell = Auth::user()->is_seller;
        if ($checkSell != 1 )return response(['error', 'user not seller'], '403');

        if ($product->where(['user_id' => Auth::id(), 'name'=>$request->name])->first()) return response(['error'=> 'duplicate name'], 415);
        $request = $product->savePro(Auth::id(),$request->name, $request->type, $request->dec, $request->imgPath, $request->count, $request->price);

        return response(['data' => $request], 200);

    }
}
