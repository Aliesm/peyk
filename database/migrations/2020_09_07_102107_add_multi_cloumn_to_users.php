<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultiCloumnToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('level')->default(1)->after('email')->comment('1:user 2:admin');
            $table->integer('is_seller')->default(0)->after('level');
            $table->string('number')->after('is_seller')->unique()->nullable();
            $table->bigInteger('cart_id')->after('number')->nullable();
            $table->integer('national_code')->after('cart_id')->nullable()->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('level');
            $table->dropColumn('is_seller');
            $table->dropColumn('number');
            $table->dropColumn('cart_id');
            $table->dropColumn('national_code');
        });
    }
}
