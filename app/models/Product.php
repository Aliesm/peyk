<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'type', 'dec', 'imgPath', 'count', 'price', 'created_at', 'updated_at'];
    protected $table = 'table_product';

    public function savePro($user_id, $name, $type, $dec, $imgPath, $count, $price)
    {
        $this->user_id  = $user_id;
        $this->name     = $name;
        $this->type     = $type;
        $this->dec      = $dec;
        $this->imgPath  = $imgPath;
        $this->count    = $count;
        $this->price    = $price;
        $this->save();
        return $this;
    }

}
