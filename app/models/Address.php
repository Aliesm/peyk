<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['user_id', 'city', 'area', 'district', 'str', 'number', 'latitude', 'longitude',];
    protected $table = 'address';


    public function saveAddress($user_id, $city, $area, $district, $str, $number, $latitude, $longitude)
    {
        $this->user_id  = $user_id;
        $this->city     = $city;
        $this->area     = $area;
        $this->district = $district;
        $this->str      = $str;
        $this->number   = $number;
        $this->number   = $number;
        $this->latitude = $latitude;
        $this->longitude= $longitude;
        $this->save();
        return $this;
    }
}
