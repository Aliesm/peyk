<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\models\Address;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserAddressController extends Controller
{
    public function store(Request $request, Address  $address)
    {

        $validate = Validator::make($request->all(),[
                    'city'  => 'required',
                    'area'  => 'required',
                    'user_id' => 'required',
                    'district' => 'required',
                    'str' => 'required',
                    'number' => 'required',
        ]);

        if ($validate->errors()) return response(['errors', $validate->errors()], 403);



        $id      = Auth::id();
        $request = $address->saveAddress($id, $request->city, $request->area, $request->district, $request->str, $request->number);
        return response(['data'=>$request], 200);


    }
}
